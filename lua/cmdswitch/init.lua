local M = {}

function M.setup(user_config)
	require("cmdswitch.config").setup(user_config)
	local options = require("cmdswitch.config").config
	if options.keymaps ~= false then
		require("cmdswitch.cmdswitch").create_keymap()
	end
	require("cmdswitch.cmdswitch").create_autocmd()
end

return M
