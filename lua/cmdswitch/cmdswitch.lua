local M = {}

local switches = require("cmdswitch.config").config.switches

local function find_switch()
	local cmd = vim.fn.getcmdline():match("^%S+")
	for group, cmds in pairs(switches) do
		for idx, c in ipairs(cmds) do
			if cmd:match("^" .. vim.pesc(c)) then
				return vim.pesc(c), group, idx
			end
		end
	end
	return nil, nil, nil
end

M.currSwitch = { group = nil, idx = nil }

local get_new_cmd = function(next)
	local cmd = vim.fn.getcmdline():match("^%S+")
	local group, idx = M.currSwitch.group, M.currSwitch.idx
	if group == nil or idx == nil or not switches[group][idx]:match("^" .. vim.pesc(cmd)) then
		_, group, idx = find_switch()
	end
	if group == nil or idx == nil then
		return nil
	end
	local newidx = (idx - 2) % #switches[group] + 1
	if next then
		newidx = (idx % #switches[group]) + 1
	end
	M.currSwitch = { group = group, idx = newidx }
	local currcmd = switches[group][idx]
	return vim.fn.getcmdline():gsub(vim.pesc(currcmd), vim.pesc(switches[group][newidx]), 1)
end

function M.cycle_cmd(next)
	next = next or false
	local cmdline = vim.fn.getcmdline()
	local currcmd = cmdline:match("^%S+")
	local cmd = get_new_cmd(next)
	if cmd == nil then
		return
	end
	-- local newcmdline = cmdline:gsub(vim.pesc(currcmd), vim.pesc(cmd), 1)
	vim.fn.setcmdline(cmd)
end

function M.create_autocmd()
	vim.api.nvim_create_autocmd({ "CmdlineEnter" }, {
		group = vim.api.nvim_create_augroup("CmdSwitchReset", {}),
		pattern = "*",
		callback = function()
			M.currSwitch = { group = nil, idx = nil }
		end,
	})
end

function M.create_keymap()
	vim.keymap.set("c", "<M-[>", function()
		require("cmdswitch.cmdswitch").cycle_cmd(false)
	end)
	vim.keymap.set("c", "<M-]>", function()
		require("cmdswitch.cmdswitch").cycle_cmd(true)
	end)
end

return M
