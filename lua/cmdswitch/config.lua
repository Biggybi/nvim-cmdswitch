local M = {}

local switches = {
	edit_short = { "e", "sp", "vs", "tabe" },
	edit_normal = { "edit", "split", "vsplit", "tabedit" },
	edit_advanced = {
		"aboveleft",
		"belowright",
		"topleft",
		"botright",
		"rightbelow",
		"leftabove",
		"vertical",
		"horizontal",
	},
	find = { "find", "sfind", "tabfind", "cfind", "lfind", "pfind", "tfind" },
	write = { "write", "update", "saveas" },
	write_short = { "w", "up", "save" },
	write_advanced = { "w", "wall", "up", "save", "wq", "wqa", "wqall", "wqa!" },
	cd = { "cd", "lcd", "tcd" },
	ls = { "ls", "args", "files", "tags" },
	grep = { "grep", "lgrep", "vimgrep", "grepadd", "lgrepadd", "vgrepadd" },
	quickfix = { "copen", "cclose", "cnext", "cprev", "cfirst", "clast" },
	help = { "Man", "h" },
	searchrange = { "s", "%s", "'<,'>s", "g", "'<,'>g" },
	search = { "/", "?" },
}

local defaults = {
	switches = switches,
	keymaps = {
		next = "<M-]>",
		prev = "<M-[>",
	},
}

M.config = defaults

function M.setup(user_options)
	M.config = vim.tbl_extend("force", defaults, user_options or {})
end

return M
